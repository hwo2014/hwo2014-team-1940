##
# Jussi Salin / JCP (C) 2014
# Hello World Open 2014 Coding World Championships
# Race car AI
#
# Revision 1.0 - 17.4.2014
# Plans good speeds to aim for in curves and throttles to reach those. Doesn't
# do yet lane switches, slowing ahead of corner or managing of drifts.
#
# Revision 1.1 - 20.4.2014
# Plans variables for each lane independently but doesn't switch lanes yet.
##

import json
import socket
import sys

# added
import math


class NoobBot(object):
    myColor = ""
    pieces = []
    lanes = []
    myCurrentSpeed = 0
    myLastPieceIndex = 0
    myLastInPieceDistance = 0
    myTotalDistance = 0
    
    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def joinRace(self):
        return self.msg("joinRace", {"botId": {"name": self.name, "key": self.key},
        "trackName": "germany",
        "password": "hmm",
        "carCount": 1})

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        #self.join()
        self.joinRace()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def on_car_positions(self, data):
        throttle = 0
        for position in data:
            if (position["id"]["color"] == self.myCarColor):
                myCurrentLane = position["piecePosition"]["lane"]["startLaneIndex"]
                myFutureLane = position["piecePosition"]["lane"]["endLaneIndex"]

                #get piece and next piece
                pieceIndex = position["piecePosition"]["pieceIndex"]
                piece = self.pieces[pieceIndex]
                nextPiece = self.pieces[0]
                if (pieceIndex + 1 < len(self.pieces)):
                    nextPiece = self.pieces[pieceIndex+1]
                
                #get my speed (length units per tics) and travelled distance
                inPieceDistance=position["piecePosition"]["inPieceDistance"]
                if (self.myLastPieceIndex == pieceIndex):
                    self.myCurrentSpeed = inPieceDistance - self.myLastInPieceDistance
                self.myLastPieceIndex = pieceIndex
                self.myLastInPieceDistance = inPieceDistance
                self.myTotalDistance = self.myTotalDistance + self.myCurrentSpeed

                #length to next piece
                pieceDistanceLeft = piece["plannedLength"][myCurrentLane] - inPieceDistance
                
                #scan for the next piece ahead that requires lower speed - 
                #then decide if it's yet time to start slowing down in this piece
                distanceToSlowerPiece = pieceDistanceLeft
                slowerPiece = piece
                for i in range(pieceIndex+1,len(self.pieces)-1)+range(0,len(self.pieces)-1):
                    if (self.pieces[i]["plannedSpeed"][myFutureLane] < piece["plannedSpeed"][myFutureLane]):
                        slowerPiece = self.pieces[i]
                        break
                    distanceToSlowerPiece = distanceToSlowerPiece + self.pieces[i]["plannedLength"][myFutureLane]
                
                #choose if to use next pieces planned speed already
                # if it's slower!
                plannedSpeed = piece["plannedSpeed"][myFutureLane]
                speedDifference = self.myCurrentSpeed - slowerPiece["plannedSpeed"][myFutureLane]
                #print("speed difference: {:.2f}".format(speedDifference))
                if (distanceToSlowerPiece < speedDifference*38): #40
                    #print("Using speed of next piece")
                    plannedSpeed = slowerPiece["plannedSpeed"][myFutureLane]
                
                # choose throttle value
                if (plannedSpeed >= self.myCurrentSpeed): 
                    throttle = 1
                elif (plannedSpeed < self.myCurrentSpeed): 
                    throttle = 0
                    
                print("[{}/{}] Speed {:.2f}, Planned {:.2f}, Distance to slower {:.2f}, Throttle {:.2f}, Lane {}->{}".format(pieceIndex, len(self.pieces), self.myCurrentSpeed, plannedSpeed, distanceToSlowerPiece, throttle, myCurrentLane, myFutureLane))
                    
        self.throttle(throttle)

    def on_crash(self, data):
        print("!!!! SOMEONE CRASHED !!!!")
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()
        
    def on_your_car(self, data):
        self.myCarColor = data["color"]
        print("My car's color is {0}".format(self.myCarColor))
        self.ping()
    
    def on_game_init(self, data):
        print("Game init")
        #print(data)
        print("Track name: "+data["race"]["track"]["name"])
        print("Number of pieces: {0}".format(len(data["race"]["track"]["pieces"])))
        print("Number of lanes: {0}".format(len(data["race"]["track"]["lanes"])))
        print("Number of opponents: {0}".format(len(data["race"]["cars"])))
        print("Number of laps: {0}".format(data["race"]["raceSession"]["laps"]))
        print("Number of lanes: {}".format(len(data["race"]["track"]["lanes"])))
        print("Max time on lap (ms): {0}".format(data["race"]["raceSession"]["maxLapTimeMs"]))
        
        self.pieces = data["race"]["track"]["pieces"]
        self.lanes = data["race"]["track"]["lanes"]
        
        for piece in self.pieces:
            piece["plannedLength"] = []
            piece["plannedAngle"] = []
            piece["plannedRadius"] = []
            piece["plannedSpeed"] = []

            for i in range(0,len(self.lanes)):
                angle = 0
                length = 0
                radius = 0

                if ("length" in piece):
                    length = piece["length"]
                    angle = 0
                    radius = 0
                elif ("radius" in piece) and ("angle" in piece):
                    angle = piece["angle"]
                    radius = piece["radius"] + self.lanes[i]["distanceFromCenter"]
                    length = abs(angle) / 360 * 2 * math.pi * radius
                    
                else:
                    print("warning: encountered a weird piece")

                piece["plannedLength"].append(length)
                piece["plannedAngle"].append(angle)
                piece["plannedRadius"].append(radius)

        for piece in self.pieces:
            for i in range(0,len(self.lanes)):
                if (piece["plannedRadius"][i] > 0): piece["plannedSpeed"].append(math.sqrt(piece["plannedRadius"][i] * 0.435)) #0.43
                else: piece["plannedSpeed"].append(9000)
            
        print (self.pieces)   
        self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'yourCar': self.on_your_car, #added
            'gameInit': self.on_game_init, #added
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            #print("got message of type:'"+msg_type+"'")
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got unhandled message: {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        bot.run()
